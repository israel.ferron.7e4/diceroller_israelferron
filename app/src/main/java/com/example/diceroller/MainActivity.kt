package com.example.diceroller

import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    //Declarem els butons
    lateinit var rollButton : Button
    lateinit var resetButton : Button
    lateinit var dau1 : ImageButton
    lateinit var dau2 : ImageButton

    //Decalrem el text la duració i la array de imatges
    var daus = intArrayOf(R.drawable.dice_1,R.drawable.dice_2,R.drawable.dice_3,R.drawable.dice_4,R.drawable.dice_5,R.drawable.dice_6)
    val text = "JAKPOT"
    val duration = Toast.LENGTH_SHORT

    //Decalrem el numero aleatori
    var n1 = 0
    var n2 = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val sound : MediaPlayer = MediaPlayer.create(this,R.raw.dado)
        rollButton = findViewById(R.id.rollButton)
        resetButton = findViewById(R.id.resetButton)
        dau1 = findViewById(R.id.dau1)
        dau2 = findViewById(R.id.dau2)


        rollButton.setOnClickListener() {
            n1 = daus.random()
            n2 = daus.random()
            sound.start()
            dau1.setImageResource(n1)
            dau2.setImageResource(n2)
            if (n1 == R.drawable.dice_6 && n2 == R.drawable.dice_6)
                Toast.makeText(this,text,duration).show()
        }

        resetButton.setOnClickListener() {
            dau1.setImageResource(R.drawable.empty_dice)
            dau2.setImageResource(R.drawable.empty_dice)
        }

        dau1.setOnClickListener{
            n1 = daus.random()
            sound.start()
            dau1.setImageResource(n1)
            if (n1 == R.drawable.dice_6 && n2 == R.drawable.dice_6)
                Toast.makeText(this,text,duration).show()
        }

        dau2.setOnClickListener{
            n2 = daus.random()
            sound.start()
            dau2.setImageResource(n2)
            if (n1 == R.drawable.dice_6 && n2 == R.drawable.dice_6)
                Toast.makeText(this,text,duration).show()
        }
    }
}
